#!/bin/sh

# Perform chroot activities on the mounted primary filesystem
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

# Set up a temporary /tmp

mount -t mfs -o async,noatime,nodev,noexec,-s256M swap ${BUILDPATH}/fs/tmp

# Run fw_update unless told to skip, include intel-firmware regardless

echo 'Running fw_update'
echo 'nameserver 8.8.8.8' >> ${BUILDPATH}/fs/etc/resolv.conf
echo 'lookup file bind' >> ${BUILDPATH}/fs/etc/resolv.conf

if [ -z "${skipfw+1}" ]; then
  if ! chroot ${BUILDPATH}/fs fw_update -a >> \
  ${BUILDPATH}/02.mkchroot.00.fw_update 2>&1; then
    echo "*** WARNING: fw_update failed, check" \
    "${BUILDPATH}/02.mkchroot.00.fw_update for connectivity and binary" \
    "compatibility. ***"
    if ls -f /var/db/pkg/|fgrep -q -- -firmware-; then
      echo "*** Falling back to copying installed firmware packages. ***"
      cp -fPpR /etc/firmware/* ${BUILDPATH}/fs/etc/firmware
      cp -fPpR /var/db/pkg/*-firmware-* ${BUILDPATH}/fs/var/db/pkg
    fi
  fi
elif [ ${MACHINE} == 'amd64' ] || [ ${MACHINE} == 'i386' ]; then
  if ! chroot ${BUILDPATH}/fs fw_update intel-firmware >> \
  ${BUILDPATH}/02.mkchroot.00.fw_update 2>&1; then
    echo "*** WARNING: fw_update failed, check" \
    "${BUILDPATH}/02.mkchroot.00.fw_update for connectivity and binary" \
    "compatibility. ***"
    if ls -f /var/db/pkg/|fgrep -q intel-firmware; then
      echo "*** Falling back to copying installed firmware packages. ***"
      mkdir -p ${BUILDPATH}/fs/etc/firmware/intel
      cp -fPpR /etc/firmware/intel/* ${BUILDPATH}/fs/etc/firmware/intel
      cp -fPpR /var/db/pkg/intel-firmware-* ${BUILDPATH}/fs/var/db/pkg
    fi
  fi
fi

rm ${BUILDPATH}/fs/etc/resolv.conf

# Install packages from pkgdir, if directed

if [ -n "${pkgdir+1}" ]; then
  echo "Installing packages: ${pkgdir}"
  mkdir -p ${BUILDPATH}/fs/tmp/pkg
  cp ${pkgdir}/*.tgz ${BUILDPATH}/fs/tmp/pkg

  if ! chroot ${BUILDPATH}/fs sh -c 'ldconfig /usr/X11R6/lib /usr/local/lib; pkg_add -v -D unsigned /tmp/pkg/*.tgz' \
  >> ${BUILDPATH}/02.mkchroot.01.pkg_add.pkgdir 2>&1; then
    echo "*** WARNING: Package installation failed, check" \
    "${BUILDPATH}/02.mkchroot.01.pkg_add.pkgdir for binary compatibility. ***"
  fi

  rm ${BUILDPATH}/fs/var/run/ld.so.hints
fi

# Install packages from pkgpath/pkglist, if directed

if [ -n "${pkgpath+1}" ] && [ -n "${pkglist+1}" ]; then
  echo "Installing packages: ${pkgpath}"
  echo 'nameserver 8.8.8.8' >> ${BUILDPATH}/fs/etc/resolv.conf
  echo 'lookup file bind' >> ${BUILDPATH}/fs/etc/resolv.conf

  # Add M:Tier signify key if found in pkgpath and present on site
  if echo ${pkgpath}|fgrep -q stable.mtier.org; then
    keyver=$(echo ${pkgpath}|tr : '\n'|fgrep stable.mtier.org|head -n 1|\
             cut -d / -f 5|tr -d .)
    ftp -o ${BUILDPATH}/fs/etc/signify/mtier-${keyver}-pkg.pub \
    https://stable.mtier.org/mtier-${keyver}-pkg.pub >> \
    ${BUILDPATH}/02.mkchroot.02.pkg_add.mtier 2>&1 || true
  fi

  if ! chroot ${BUILDPATH}/fs sh -c "ldconfig /usr/X11R6/lib /usr/local/lib; env PKG_PATH=${pkgpath} pkg_add -v $(echo ${pkglist}|tr , ' ')" \
  >> ${BUILDPATH}/02.mkchroot.03.pkg_add.pkgpath 2>&1; then
    echo "*** WARNING: Package installation failed, check" \
    "${BUILDPATH}/02.mkchroot.03.pkg_add.pkgpath for connectivity and version" \
    "compatibility. ***"
  fi

  rm ${BUILDPATH}/fs/etc/resolv.conf ${BUILDPATH}/fs/var/run/ld.so.hints
fi

# Clean up

umount ${BUILDPATH}/fs/tmp
